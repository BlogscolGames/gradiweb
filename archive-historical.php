<?php
get_header();

?>

<main id="site-content" role="main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p class="font-weight-bold pt-2 mb-1 text-center"><?php echo post_type_archive_title(); ?></p>
			</div>

			<div class="col-md-12 posts">
				<div class="row itemsListado">
					<?php

					if ( have_posts() )
					{
						while ( have_posts() )
						{
							the_post();

							$title = explode(' ', get_the_title());
							$Ciudad = $title[0];
							
							$HoraTemp = date("g:i A", strtotime($title[2]));
							$HoraTemp = explode(' ', $HoraTemp);

							$Hora = $HoraTemp[0];
							$AM_PM = $HoraTemp[1];

							$Fecha = date('l, j F Y', strtotime($title[1]));
							$Clima = get_post_meta( get_the_ID() , 'main', true );

							$fondo = 'https://source.unsplash.com/featured/?nature,' . sanitize_title(get_post_meta( get_the_ID() , 'main', true )) . '.png';

							?>
								<div class="container-fluid px-1 px-md-4 py-5 mx-auto">
								    <div class="row d-flex justify-content-center px-3">
								        <div class="card" style="background: linear-gradient(rgba(0,0,0,.5), rgba(0,0,0,.5)),url('<?php echo $fondo; ?>') no-repeat center center /cover ">

								            <h2 class="ml-auto mr-4 mt-3 mb-0 med-font"><?php echo $Ciudad; ?></h2>

								            <h2 class="ml-auto mr-4 mt-3 mb-0 med-font"><?php echo $Clima; ?></h2>

								            <p class="ml-auto mr-4 mb-0 med-font"></p>
								            <h1 class="ml-auto mr-4 med-font"><?php echo get_post_meta( get_the_ID() , 'temp', true )?>&#176;</h1>
								            <p class="time-font mb-0 ml-4 mt-auto"><?php echo $Hora; ?> <span class="sm-font"><? echo $AM_PM; ?></span></p>
								            <p class="ml-4 mb-4"><?php echo $Fecha; ?></p>

								        </div>
								    </div>
								</div>
							<?php
						}
					}
					?>
				</div>				
			</div>
			<div class="container">
				<div class="row">
			    	<div class="col text-center posts-navigation">
	    		    	<?php next_posts_link( 'Cargar mas' ); ?>
				    </div>
				</div>
			</div>
		</div>
	</div>
</main><!-- #site-content -->

<?php get_footer(); ?>
