<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.linkedin.com/in/miguel-mariano-developer/
 * @since      1.0.0
 *
 * @package    Gradiweb
 * @subpackage Gradiweb/admin/partials
 */

    ?>
    <div class="wrap">

      <h2><?php _e( 'Gradiweb options', 'gradiweb-plugin' ); ?></h2>
      <?php settings_errors(); ?>

      <form method="post" action="options.php">
	    <?php settings_fields( 'gradiweb-plugin-settings-group' ); ?>
	    <?php do_settings_sections( 'gradiweb-plugin-settings-group' ); ?>
	    <table class="form-table">
	        <tr valign="top">
		        <th scope="row">EndPoint</th>
		        <td>
		        	<input type="text" name="endpoint" id="endpoint" value="<?php echo esc_attr( get_option('endpoint') ); ?>"/>
		        </td>
	        </tr>

	        <tr valign="top">
	        	<th scope="row">Api Key</th>
	        	<td>
	        		<input type="text" name="apikey" id="apikey" value="<?php echo esc_attr( get_option('apikey') ); ?>" />
	        	</td>
	        </tr>

	        <tr valign="top">
		        <th scope="row">City ID</th>
		        <td>
		        	<input type="text" name="cityID" id="cityID" value="<?php echo esc_attr( get_option('cityID') ); ?>" />
		        </td>
	        </tr>
	    </table>
	    
	    <?php submit_button(); ?>

	    <div class="buttonHistorical">
			<button class="btn btn-primary" type="button" name="getHistorico" id="getHistorico">
			  <span class="spinner-border spinner-border-sm d-none" role="status" id="spinner" aria-hidden="true"></span>
			  <span class="titleGuardarHistorico">Guardar historico</span>
			</button>
		</div>

		<a href="<?php echo get_post_type_archive_link('historical'); ?>">Ver pagina historicos</a>
      </form>

    </div><!-- /.wrap -->    <?php



?>