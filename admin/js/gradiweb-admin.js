(function( $ ) {
	'use strict';

	$( window ).load(function()
	{
	 	var endpoint = $('#endpoint').val();
	 	var apikey = $('#apikey').val();
	 	var cityID = $('#cityID').val();

	 	if (endpoint != "" && apikey != "" && cityID != "")
	 	{
	 		$('.buttonHistorical').css("display", "block");
	 	}

	    $('#getHistorico').click(function(e)
	    {
	        e.preventDefault();

   			$("#spinner").removeClass('d-none');
	        $('#getHistorico').prop('disabled', true);
			$(".titleGuardarHistorico").html("Guardando");

 	       	$.ajax(
	        {
	            method: "GET",
	            url: ajaxurl,
				data: {"action": "getData", "endpoint": endpoint, "apikey" : apikey, "cityID" : cityID},
	            success : function(data)
	            {
		   			$("#spinner").addClass('d-none');
					$('#getHistorico').prop('disabled', false);
					$(".titleGuardarHistorico").html("Guardar historico");
	            },
	            error: function(data)
	            {
		   			$("#spinner").addClass('d-none');
	            	$('#getHistorico').prop('disabled', false);
					$(".titleGuardarHistorico").html("Guardar historico");
	            },
	            fail: function(data)
	            {
		   			$("#spinner").addClass('d-none');
	            	$('#getHistorico').prop('disabled', false);
					$(".titleGuardarHistorico").html("Guardar historico");
	            }
	        });
	    });
	});
})( jQuery );