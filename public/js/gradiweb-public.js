(function( $ ) {
	'use strict';

	$( window ).load(function()
	{
		$('.posts-navigation a').click(function(e)
		{
    		e.preventDefault();
 
 			var page = $(this).attr('href');

		    $(this).text('Loading...').addClass('loading');

        	$('.posts-navigation a').hide();

    		$.get(page, function (data)
    		{
		    	var items = $(data).find('.itemsListado');
		    	console.log(items);

		    	var nextpage = $(data).find('.posts-navigation a').attr('href');
		      	var container = $('.posts');

		      	container.append(items);

		      	$('.posts-navigation a').text('Load More').attr('href', nextpage);

		      	if (nextpage !== undefined)
		      	{
		        	$('.posts-navigation a:last').show();
		      	}
		    });
   	    });
  	});
})( jQuery );