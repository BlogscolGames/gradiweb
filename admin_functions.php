<?php
	function register_historical_posts()
	{
	    $labels = array
	    (
	        'name'                => _x( 'Historicos', 'Post Type General Name', 'historicos' ),
	        'singular_name'       => _x( 'Historico', 'Post Type Singular Name', 'historicos' ),
	        'menu_name'           => __( 'Historicos', 'historicos' ),
	        'parent_item_colon'   => __( 'Parent Historico', 'historicos' ),
	        'all_items'           => __( 'All Historicos', 'historicos' ),
	        'view_item'           => __( 'View Historico', 'historicos' ),
	        'add_new_item'        => __( 'Add New Historico', 'historicos' ),
	        'add_new'             => __( 'Add New', 'historicos' ),
	        'edit_item'           => __( 'Edit Historico', 'historicos' ),
	        'update_item'         => __( 'Update Historico', 'historicos' ),
	        'search_items'        => __( 'Search Historico', 'historicos' ),
	        'not_found'           => __( 'Not Found', 'historicos' ),
	        'not_found_in_trash'  => __( 'Not found in Trash', 'historicos' ),
	    );
	      
		$args = array
		(
			'labels'             => $labels,
			'description'        => __( 'Historical', 'historicos' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'historicos' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 4,
	    	'menu_icon'           => plugins_url() . '/gradiweb/admin/icons/historical.png',
			'supports'           => array( 'title', 'thumbnail' )
		);

		register_post_type( 'historical', $args );
		flush_rewrite_rules();
	}

	add_filter('template_include', 'historical_template');
	function historical_template( $template )
	{
		if ( is_post_type_archive('historical') )
	  	{
			return plugin_dir_path(__FILE__) . 'archive-historical.php';
		}

		return $template;
	}

	add_action( 'add_meta_boxes', 'historical_meta_box_add' );
	function historical_meta_box_add()
	{
	  add_meta_box( 'historical-meta-box', 'Data', 'historical_meta_box_cb', ['historical'], 'normal', 'high' );
	}

	function historical_meta_box_cb( $post )
	{
		$values = get_post_custom( $post->ID );

		$temp = isset( $values['temp'] ) ? esc_attr( $values['temp'][0] ) : '';
		$temp_min = isset( $values['temp_min'] ) ? esc_attr( $values['temp_min'][0] ) : '';
		$temp_max = isset( $values['temp_max'] ) ? esc_attr( $values['temp_max'][0] ) : '';
		$pressure = isset( $values['pressure'] ) ? esc_attr( $values['pressure'][0] ) : '';

		$humidity = isset( $values['humidity'] ) ? esc_attr( $values['humidity'][0] ) : '';
		$id = isset( $values['id'] ) ? esc_attr( $values['id'][0] ) : '';
		$main = isset( $values['main'] ) ? esc_attr( $values['main'][0] ) : '';
		$description = isset( $values['description'] ) ? esc_attr( $values['description'][0] ) : '';
	    ?>
	    
	    <div class="">
			<p>
		    	<label for="temp"><h2><span style="font-size: 20px;">Temperatura</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="temp" id="temp" value="<?php echo $temp; ?>"/>
			</p>
		</div>    

	    <div class="">
			<p>
		    	<label for="temp_min"><h2><span style="font-size: 20px;">Temperatura Minima</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="temp_min" id="temp_min" value="<?php echo $temp_min; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="temp_max"><h2><span style="font-size: 20px;">Temperatura Maxima</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="temp_max" id="temp_max" value="<?php echo $temp_max; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="pressure"><h2><span style="font-size: 20px;">Presión</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="pressure" id="pressure" value="<?php echo $pressure; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="humidity"><h2><span style="font-size: 20px;">Humedad</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="humidity" id="humidity" value="<?php echo $humidity; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="id"><h2><span style="font-size: 20px;">ID</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="id" id="id" value="<?php echo $id; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="main"><h2><span style="font-size: 20px;">Clima</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="main" id="main" value="<?php echo $main; ?>"/>
			</p>
		</div>

	    <div class="">
			<p>
		    	<label for="description"><h2><span style="font-size: 20px;">Descripción</span></h2></label>
		    </p>
		    <p>
		    	<input type="text" name="description" id="description" value="<?php echo $description; ?>"/>
			</p>
		</div>

		<?php
	}

	add_action( 'save_post', 'historical_meta_box_save' );
	function historical_meta_box_save( $post_id )
	{
	    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

		$temp = isset( $values['temp'] ) ? esc_attr( $values['temp'][0] ) : '';
		$temp_min = isset( $values['temp_min'] ) ? esc_attr( $values['temp_min'][0] ) : '';
		$temp_max = isset( $values['temp_max'] ) ? esc_attr( $values['temp_max'][0] ) : '';
		$pressure = isset( $values['pressure'] ) ? esc_attr( $values['pressure'][0] ) : '';

		$humidity = isset( $values['humidity'] ) ? esc_attr( $values['humidity'][0] ) : '';
		$id = isset( $values['id'] ) ? esc_attr( $values['id'][0] ) : '';
		$main = isset( $values['main'] ) ? esc_attr( $values['main'][0] ) : '';
		$description = isset( $values['description'] ) ? esc_attr( $values['description'][0] ) : '';

	    if( isset( $_POST['temp'] ) )
	        update_post_meta( $post_id, 'temp', $_POST['temp'] );

	    if( isset( $_POST['temp_min'] ) )
	        update_post_meta( $post_id, 'temp_min', $_POST['temp_min'] );

	    if( isset( $_POST['temp_max'] ) )
	        update_post_meta( $post_id, 'temp_max', $_POST['temp_max'] );

	    if( isset( $_POST['pressure'] ) )
	        update_post_meta( $post_id, 'pressure', $_POST['pressure'] );

	    if( isset( $_POST['humidity'] ) )
	        update_post_meta( $post_id, 'humidity', $_POST['humidity'] );

	    if( isset( $_POST['id'] ) )
	        update_post_meta( $post_id, 'id', $_POST['id'] );

	    if( isset( $_POST['main'] ) )
	        update_post_meta( $post_id, 'main', $_POST['main'] );

	    if( isset( $_POST['description'] ) )
	        update_post_meta( $post_id, 'description', $_POST['description'] );
	}

	function gradiweb_enqueue_scripts()
	{
	    wp_localize_script('gradiweb_js', 'gradiweb_js', 
        	array
        	( 
            	'ajaxurl' => admin_url( 'admin-ajax.php' ) 
        	)
    	);

    	wp_enqueue_script( 'gradiweb_js' );
	}
	add_action( 'wp_enqueue_scripts', 'gradiweb_enqueue_scripts' );

	function gradiweb_ajaxurl()
	{ 
	    ?>
	    <script type="text/javascript"> //<![CDATA[
	        ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
	    //]]> </script>
	    <?php 
	}
	add_action( 'wp_head', 'gradiweb_ajaxurl', 1 );

    function getData()
    {
    	$data = getCoordenadas($_GET['cityID'], $_GET['apikey']);
		
		$historico = getHistorico($data["lat"], $data["lon"], time(), $_GET['apikey']);
		
		return $historico;

		exit;
    }
	add_action( 'wp_ajax_getData', 'getData' );
	add_action( 'wp_ajax_nopriv_getData', 'getData' );

	function getCoordenadas($cityID, $apiKey)
	{
  		/*
	  		La version FREE no permite usar el endpoint "https://openweathermap.org/history", la unica forma de sacar un historico con la version FREE fue usando este otro Endpoint "https://openweathermap.org/api/one-call-api#history"

	  		Pero este solo funciona con Latitud y Longitud no con el CityID, para obtener estos datos primero use el Current endpoint "https://openweathermap.org/current"
	  		
			Ya con la Latitud y Longitud si pude llamar al "https://openweathermap.org/api/one-call-api#history" y sacar los datos del historico
		*/

  		header("Access-Control-Allow-Origin: *");

  		$currentData = 'http://api.openweathermap.org/data/2.5/weather?id=' . $cityID . '&APPID=' . $apiKey;

		$ch = curl_init($currentData);
	  	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

	  	$result = curl_exec($ch);

	  	if($result === false)
	  		return false;

		$data = json_decode($result, true);

	  	return $data["coord"];
	}

	function getHistorico($lat, $lon, $timestamp, $apiKey)
	{
  		header("Access-Control-Allow-Origin: *");

  		$historicalData = 'https://api.openweathermap.org/data/2.5/onecall?lat=' . $lat . '&lon=' . $lon . '&dt=' . $timestamp . '&exclude=hourly,minutely,alerts&lang=sp&units=metric&APPID=' . $apiKey;

		$ch = curl_init($historicalData);
	  	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

	  	$result = curl_exec($ch);

	  	if($result === false)
	  		return false;

	  	$data = json_decode($result, true);

		save_historical_data_API($data);
	}

	function save_historical_data_API( $data )
	{
		for($i=0; $i<12; $i++)
		{
			$title = $data["timezone"] . ' ' . date('m/d/Y H:i:s',$data["hourly"][$i]["dt"]);

			$post = get_post_by_title($title, 'historical');

			if($post == null)
			{
				$new_post = array(
			      'ID' => '',
			      'post_type' => 'historical',
			      'post_status' => 'publish',
			      'post_title' => $title
			    );

				$post_id = wp_insert_post($new_post);

		        update_post_meta( $post_id, 'temp', $data["hourly"][$i]["temp"] );
		        update_post_meta( $post_id, 'temp_min', $data["hourly"][$i]["temp_min"] );//Este dato no lo retorna el API
		        update_post_meta( $post_id, 'temp_max', $data["hourly"][$i]["temp_max"] );//Este dato no lo retorna el API
		        update_post_meta( $post_id, 'pressure', $data["hourly"][$i]["pressure"] );
		        update_post_meta( $post_id, 'humidity', $data["hourly"][$i]["humidity"] );
		        update_post_meta( $post_id, 'id', $data["hourly"][$i]["weather"][0]["id"] );
		        update_post_meta( $post_id, 'main', $data["hourly"][$i]["weather"][0]["main"] );
		        update_post_meta( $post_id, 'description', $data["hourly"][$i]["weather"][0]["description"]);
			}
			else
			{
				if($data["hourly"][$i]["humidity"] < 90)
					wp_delete_post($post->ID);
				else
				{					
			        update_post_meta( $post->ID, 'temp', $data["hourly"][$i]["temp"] );
			        update_post_meta( $post->ID, 'temp_min', $data["hourly"][$i]["temp_min"] );//Este dato no lo retorna el API
			        update_post_meta( $post->ID, 'temp_max', $data["hourly"][$i]["temp_max"] );//Este dato no lo retorna el API
			        update_post_meta( $post->ID, 'pressure', $data["hourly"][$i]["pressure"] );
			        update_post_meta( $post->ID, 'humidity', $data["hourly"][$i]["humidity"] );
			        update_post_meta( $post->ID, 'id', $data["hourly"][$i]["weather"][0]["id"] );
			        update_post_meta( $post->ID, 'main', $data["hourly"][$i]["weather"][0]["main"] );
			        update_post_meta( $post->ID, 'description', $data["hourly"][$i]["weather"][0]["description"]);
				}
			}
		}
	}

	function get_post_by_title($page_title, $post_type ='post' , $output = OBJECT)
	{
    	global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));
        
        if ( $post )
            return get_post($post, $output);

    	return null;
	}

	function set_posts_per_page_for_historical( $query )
	{
  		if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'historical' ) )
    		$query->set( 'posts_per_page', '5' );
  	}
	add_action( 'pre_get_posts', 'set_posts_per_page_for_historical' );

?>