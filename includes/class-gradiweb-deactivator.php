<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.linkedin.com/in/miguel-mariano-developer/
 * @since      1.0.0
 *
 * @package    Gradiweb
 * @subpackage Gradiweb/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gradiweb
 * @subpackage Gradiweb/includes
 * @author     Miguel Mariano <miguel>
 */
class Gradiweb_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate()
	{
		delete_option('endpoint');
		delete_option('apikey');
		delete_option('cityID');

		unregister_setting( 'gradiweb-plugin-settings-group', 'endpoint' );
		unregister_setting( 'gradiweb-plugin-settings-group', 'apikey' );
		unregister_setting( 'gradiweb-plugin-settings-group', 'cityID' );
	}

}
