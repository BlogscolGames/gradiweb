<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.linkedin.com/in/miguel-mariano-developer/
 * @since      1.0.0
 *
 * @package    Gradiweb
 * @subpackage Gradiweb/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gradiweb
 * @subpackage Gradiweb/includes
 * @author     Miguel Mariano <miguel>
 */
class Gradiweb_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
